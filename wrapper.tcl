#! /bin/tclsh

# comment/todo blocks
proc @comment {comment} {}
proc @todo {todo} {}

@todo {
    - support with fzf and terminal input?
}

# get local directory
set LOCAL_DIR [file dirname [file normalize [info script]]]
set {script name} [file nativename "$LOCAL_DIR/snippets.tcl"]

set black #000000
set white #eeeeee

set line [ exec tclsh ${script name} --list | dmenu -l 9 -fn monospace:size=9 -nb $black -nf $white -sb $white -sf $black ]
set snippet [ exec tclsh ${script name} --snippet $line ]
exec xdotool type $snippet
