#! /bin/tclsh

# snippets is a script for managing bookmarks and other pieces of information using
# tcl + emacs org mode

# get local direcotry
set LOCAL_DIR [file dirname [file normalize [info script]]]

# comment/todo blocks
proc @comment {comment} {}
proc @todo {todo} {}

@todo {
    - add help text + doc usage
    - cleanup
    - delete a snippet
    - edit a snippet?
    - add wrapper for creating a new snippet
}

# initialise empty list to store snippets
set snippets [list]

# import conf settings from conf file
source [file nativename "$LOCAL_DIR/snippets.conf"]

proc {print array} {a {prefix ""} {upvar_lvl 1}} {
    # print an array
    upvar $upvar_lvl $a arr
    foreach key [array names arr] {
        puts "$prefix$key: $arr($key)"
    }
}

proc {print snip as line} {a {upvar_lvl 1}} {
    # print snippet as line for list view
    set url ""
    set friendly ""
    set note ""
    set tagged ""

    upvar $upvar_lvl $a arr
    foreach key [array names arr] {
        if {$key eq "url"} {
            set url $arr($key)
        }

        if {$key eq "friendly"} {
            set friendly $arr($key)
        }

        if {$key eq "note"} {
            set note $arr($key)
        }

        if {$key eq "tagged"} {
            set tagged $arr($key)
        }
    }

    puts "$url $::DELIMITER $friendly: $note; $tagged"
}

proc {load snippets} {file} {
    # load snippets from an org file
    set fp [open $file r]
    set fileData [read $fp]
    close $fp
    set snippetsData [split $fileData "\n"]

    # array set snippet {}
    set friendly ""
    set note ""
    set url ""
    set tagged ""
    
    set snipCount 0
    set loading 0

    foreach line $snippetsData {
        # check for the start of a snippet
        if {$loading == 0 && [string first {**} $line] == 0} {
            # set friendly name and remove prefix
            # set snippet(friendly) [string range $line 3 end]
            set friendly [string range $line 3 end]
            set loading 1
        }

        if {$loading == 1 && [string first {:url:} $line] == 0} {
            # set url and remove prefix
            # set snippet(url) [string range $line 6 end]
            set url [string range $line 6 end]
        }
        if {$loading == 1 && [string first {:note:} $line] == 0} {
            # set note and remove prefix
            # set snippet(note) [string range $line 7 end]
            set note [string range $line 7 end]
        }
        if {$loading == 1 && [string first {:tagged:} $line] == 0} {
            # set tags and remove prefix
            # set snippet(tagged) [string range $line 9 end]
            set tagged [string range $line 9 end]
        }

        if {$loading == 1 && [string first {:END:} $line] == 0} {
            set loading 0
            upvar $snipCount snippet
            set snippet(friendly) $friendly
            set snippet(url) $url
            set snippet(note) $note
            set snippet(tagged) $tagged
            lappend ::snippets $snipCount

            set friendly ""
            set note ""
            set url ""
            set tagged ""
            incr snipCount
        }
    }
}

proc {snippet friendly name} {snippet {upvar_lvl 1}} {
    # get the friendly name for the given snippet
    upvar $upvar_lvl $snippet snip
    return $snip(friendly)
}

proc {list snippets} {} {
    # print all snippets in list formatting (for
    # use with fzf, dmenu, rofi, etc)
    {load snippets} $::SNIP_FILE; # load the snippets
    for {set i 0} {$i < [llength $::snippets]} {incr i} {
        set snippet [lindex $::snippets $i]
        {print snip as line} $snippet
    }

}

proc {print snippets} {} {
    # print all snippets to the console
    {load snippets} $::SNIP_FILE; # load the snippets

    #print the snippets
    for {set i 0} {$i < [llength $::snippets]} {incr i} {
        set snippet [lindex $::snippets $i]
        puts "$i: [{snippet friendly name} $snippet 1]"
        {print array} $snippet "\t" 1
    }
}

proc {new snippet} {snippet} {
    # new snippet
    set friendly ""
    set note ""
    set tagged ""
    puts "Snippet:"
    puts $snippet
    puts "Friendly Name:"
    gets stdin friendly
    puts "Snippet Note:"
    gets stdin note
    puts "Snippet Tags:"
    gets stdin tagged

    set fo [open $::SNIP_FILE a]
    puts $fo "** $friendly\n:PROPERTIES:\n:url: $snippet\n:note: $note\n:tagged: $tagged\n:END:"
    close $fo
}

proc help {} {
    puts "$::argv0
a cli to manage bookmarks and other pieces of
information using tcl + emacs org mode

commands:
help            display this help text
--new-snippet   create a new snippet
--print         full print all snippets
                to the console
--list          list all snippets for
                use in dmenu, fzf, etc
--snippet       given input from --list,
                return the snippet"
}

if {!$argc} {
    help
    return
}

set command [lindex $argv 0]

@comment {
    example:

    you can use the following to use fzf to search through the snippets by tag, friendly name,
    url, and/or note
    tclsh snippets.tcl --snippet $(tclsh snippets.tcl --list | fzf)
}

switch $command {
    {--new-snippet} {
        {new snippet} [lindex $argv 1]
    }
    {--print} {
        {print snippets}; # full print for use in console
    }
    {--list} {
        {list snippets}; # for use in dmenu, rofi, fzf, etc
    }
    {--snippet} {
        set inputSnippet [lindex $argv 1]
        puts [string range $inputSnippet 0 [ string first " $::DELIMITER" $inputSnippet ]-1 ]
    }
    default {
        help
    }
}
